// axios 全局拦截器
import axios from "axios";
import router from "./router"
import { ElMessage } from "element-plus"

// 定义全局URL 涉及跨域问题
axios.defaults.baseURL = "http://localhost:8081"; // 前端将访问8081端口的后端数据

// 创建一个axios对象
const request = axios.create(
    // 统一设置一些参数
    {
        // 请求超时时间 ms
        timeout: 5000,
        headers: {
            // 告诉返回的header是一个json数据
            'Content-Type': 'application/json;charset=UTF-8',
        }
    }
);


// 拦截器 
/**
 * 发起请求先拦截，在这添加header的相关数据
 * 附上代表身份的请求头
 */
request.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // 例如：可以在这里添加请求头
    // 例如：可以在这里添加token
    // 添加token
    config.headers['Authorization'] = localStorage.getItem("token");
    return config;
});

// 返回一个结果
request.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    // 例如：可以在这里统一处理返回的数据

    // 判断返回的状态码是不是 200
    let res = response.data;
    if (res.code == 200) {
        return response;
    } else {
        // 三元表达式 如果又msg内容，优先返沪msg的内容
        ElMessage.error(!res.msg ? '系统异常' : res.msg);
        // 拒绝访问 并附上拒绝的原因
        return Promise.reject(response.data.msg || '系统异常');
    }
},
    // 其他系统异常情况 状态码异常情况
    function (error) {
        // 判断response是否有数据, 如果有数据就优先返回data中的msg
        if (error.response.data) {
            error.message = error.response.data.message;
        }
        // 对响应错误做点什么
        if (error.response.data === 401) { // 未经授权
            // 跳转到登录页面
            router.push('/login');
        }
        ElMessage.error({ // 传入的是一个对象
            message: error.message,
            duration: 5 * 1000
        });
        // 返回一个带有错误信息的Promise对象，以便调用者可以处理错误。
        return Promise.reject(error);
    }
);

// 暴露出去
export default request;