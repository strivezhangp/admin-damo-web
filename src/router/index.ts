import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";
// 导入axios
import axios from "../axios";
// 导入store状态管理
import store from "@/store";

// 路由管理
const routes: Array<RouteRecordRaw> = [
  {
    // home主路由，包含了菜单栏，header等，
    // 其中的内容展示main部分通过子路由 嵌套实现不同信息的展示
    path: "/",
    name: "Home",
    component: () => import("@/views/Home.vue"),
    // 使用子路由的方式实现index在home中的嵌套显示
    children: [
      // 主页信息展示
      {
        path: "/index",
        name: "Index",
        meta: {
          title: "首页",
        },
        component: () => import("@/views/Index.vue"),
      },
      // 个人中心
      {
        path: "/userCenter",
        name: "UserCenter",
        meta: {
          title: "个人中心",
        },
        component: () => import("@/views/UserCenter.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "Login",
    // 懒加载的方式一种， 不需要去引入
    component: () => import("../views/Login.vue"),
  },
];

const router = createRouter({
  // 创建了一个历史对象
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// 动态路由绑定 beforeEach 进行路由之前的判断
/**
 * to 要跳转到的路由
 * from 来自那个路由
 * next 让它继续往下面去走
 */
router.beforeEach((to, from, next) => {
  // 判断是否已经获取到菜单导航数据
  let hasRoute = store.state.menus.hasRoutes;
  let token = localStorage.getItem("token");

  if (to.path === "/login") {
    next();
  } else if (!token) {
    // 是否存在token
    next({ path: "/login" });
  } else if (token && !hasRoute) {
    // 获取导航数据 {}里面是请求头 用的不是全局的axios所以重写头部，实现拦截
    axios
      .get("/sys/menu/nav", {
        // 请求头
        headers: {
          Authorization: localStorage.getItem("token"), // 获取到了session的信息
        },
      })
      .then((res: any) => {
        // 拿到menuList 赋值到store中的菜单导航模块中
        store.commit("setMenuList", res.data.data.nav);

        //拿到用户权限
        store.commit("setPermList", res.data.data.authoritys);

        /**
         * 绑定动态路由
         */

        // 先拿到现有的路由
        const newRoutes = router.options.routes;
        // for循环添加路由
        res.data.data.nav.forEach((menu: any) => {
          // 判断是否有子路由
          if (menu.children) {
            menu.children.forEach((item: any) => {
              // 转成路由
              let route = menuToRoute(item);
              // 路由不为空 把路由添加到路由管理中
              if (route) {
                // 如果children不为null 再添加route
                newRoutes[0].children?.push(route);
              }
            });
          }
        });
        // 将路由添加进去
        router.addRoute(newRoutes[0]);
        // 设置是否获取路由标志为true
        hasRoute = true;
        // 全局改变后提交
        store.commit("changeHasRouter", hasRoute);
      });
  }

  // 当没有路由时请求获取
  if (!hasRoute) {
  }
  next(); // 让它继续往前面走
});

// 定义路由转换的方法
function menuToRoute(menu: any) {
  // 判断是否有component
  if (!menu.component) {
    return null;
  } else {
    // 不为空 定义一个路由对象
    let route = {
      name: menu.name,
      path: menu.path,
      meta: {
        icon: menu.icon,
        title: menu.title,
      },
      component: () => import("@/views/" + menu.component + ".vue"),
    };

    return route;
  }
}

export default router;
