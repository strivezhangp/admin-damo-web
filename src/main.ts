import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import axios from 'axios' // 原始的axios
import axios from './axios' // 自定义的 axios 实例
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 全局引入vue-icons
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// 引入全局方法
import { hasAuth } from './globalFunc';

// require("./mock") //引入mock数据，关闭则注释该行

const app = createApp(App);
// 引入全局icon图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// 使用 app.config.globalProperties 替代 Vue.prototype 来添加全局属性。
app.config.globalProperties.$axios = axios;
// 添加全局方法
app.config.globalProperties.$hasAuth = hasAuth;

app.use(router);
app.use(store);
app.use(ElementPlus);

app.mount('#app');
