const Mock = require("mockjs");
// 生成随机验证码
const Random = Mock.Random; // 拿到Random对象

/**
 * ts 特性，先定义结构 后实例化对象
 */

// 定义验证码的 返回的数据格式接口
interface ResultData {
  token: string;
  captchaImg: string;
}

// 统一封装用户信息数据格式接口
interface UserInfoData {
  id: string;
  username: string;
  avatar: string;
}

// 统一封装动态菜单绑定数据接口
interface MenuData {
  nav: any[];
  authoritys: any[];
}

// 统一封装菜单表格数据接口
interface TableData {
  id: number;
  created?: string;
  updated?: string;
  statu: number;
  parentId: number;
  name: string;
  perms: string;
  component?: string;
  type: number;
  path?: string;
  icon?: string;
  orderNum: number;
  children: TableData[];
}

// 用户角色数据格式
interface Role {
  id: number;
  created: string;
  updated: string;
  statu: number;
  name: string;
  code: string;
  describe: string;
  menuIds: number[];
}

interface RoleListResponse {
  records: Role[];
  total: number;
  size: number;
  current: number;
  orders: any[]; // 假设orders可以是任意类型的数组
  optimizeCountSql: boolean;
  hitCount: boolean;
  countId: null;
  maxLimit: null;
  searchCount: boolean;
  pages: number;
}

interface RoleInfoResponse {
  id: number;
  created: string;
  updated: string;
  statu: number;
  name: string;
  code: string;
  describe: string;
  menuIds: number[];
}

// 统一封装后端返回的数据格式
interface Result {
  code: number;
  msg: string;
  data:
  | ResultData
  | UserInfoData
  | MenuData
  | TableData
  | Role
  | RoleInfoResponse
  | RoleListResponse
  | ListInfo
  | UserInfo
  | null;
}

// 请求路径 方法 模拟后端返回信息
Mock.mock("/captcha", "get", () => {
  // 模拟后端返回的数据 同时实例化一个result对象
  const result: Result = {
    code: 200,
    msg: "成功！",
    data: {
      token: Random.string(32), // 生成32位随机数
      captchaImg: Random.dataImage("120x40", "1111"),
    },
  };
  // 返回一个结果对象
  return result;
});

Mock.mock("/login", "post", function () {
  // 正常登录的情况
  const result: Result = {
    code: 200,
    msg: "登录成功！",
    data: null,
  };

  // 模拟异常登录的情况
  // const result: Result = {
  //     code: 400, // 错误码
  //     msg: "登录异常！", // 错误信息
  //     data: null // 数据为空
  // }
  // 返回一个结果对象
  return result;
});

// 模拟获取用户信息
Mock.mock("/sys/userInfo", "get", () => {
  // 正常情况下
  const result: Result = {
    code: 200,
    msg: "获取用户信息成功！",
    data: {
      id: Random.id(), // 生成随机id
      username: Random.cname(), // 生成随机中文名字
      avatar: Random.image("100x100", "#000", "#FFF", "avatar"), // 生成随机头像
    },
  };
  return result;
});

// 模拟修改密码成功
Mock.mock("/sys/user/updatepassword", "post", () => {
  const result: Result = {
    code: 200,
    msg: "修改密码成功！",
    data: null,
  };
  return result;
});

// 模拟用户登出
Mock.mock("/logout", "post", () => {
  const result: Result = {
    code: 200,
    msg: "退出登录成功！",
    data: null,
  };
  return result;
});

// 模拟用户动态菜单数据
Mock.mock("/sys/menu/nav", "post", () => {
  // 菜单参数与路由动态绑定
  let nav = [
    {
      name: 'SysManga',
      title: '系统管理',
      icon: 'Operation',
      component: '',
      path: '',
      children: [
        {
          name: 'SysUser',
          title: '用户管理',
          icon: 'User',
          path: '/sys/users',
          component: 'sys/User',
          children: []
        },
        {
          name: 'SysRole',
          title: '角色管理',
          icon: 'Rank',
          path: '/sys/roles',
          component: 'sys/Role',
          children: []
        },
        {
          name: 'SysMenu',
          title: '菜单管理',
          icon: 'Menu',
          path: '/sys/menus',
          component: 'sys/Menu',
          children: []
        }
      ]
    },
    {
      name: "SysTools",
      title: "系统工具",
      icon: "Tools",
      path: "",
      component: "",
      children: [
        {
          name: "SysDict",
          title: "数字字典",
          icon: "Document",
          path: "/sys/dicts",
          component: "",
          children: [],
        },
      ],
    },
  ];
  let authoritys: any = ['sys:user:save']; // 用户权限信息
  const result: Result = {
    code: 200,
    msg: "获取菜单数据成功！",
    data: {
      nav: nav,
      authoritys: authoritys,
    },
  };
  return result;
});

// 菜单管理接口
Mock.mock("/sys/menu/list", "get", () => {
  let menus: any = [
    {
      id: 1,
      created: "2021-01-15T18:58:18",
      updated: "2021-01-15T18:58:20",
      statu: 1,
      parentId: 0,
      name: "系统管理",
      path: "",
      perms: "sys:manage",
      component: "",
      type: 0,
      icon: "el-icon-s-operation",
      ordernum: 1,
      children: [
        {
          id: 2,
          created: "2021-01-15T19:03:45",
          updated: "2021-01-15T19:03:48",
          statu: 1,
          parentId: 1,
          name: "用户管理",
          path: "/sys/users",
          perms: "sys:user:list",
          component: "sys/User",
          type: 1,
          icon: "el-icon-s-custom",
          ordernum: 1,
          children: [
            {
              id: 9,
              created: "2021-01-17T21:48:32",
              updated: null,
              statu: 1,
              parentId: 2,
              name: "添加用户",
              path: null,
              perms: "sys:user:save",
              component: null,
              type: 2,
              icon: null,
              ordernum: 1,
              children: [],
            },
            {
              id: 10,
              created: "2021-01-17T21:49:03",
              updated: "2021-01-17T21:53:04",
              statu: 1,
              parentId: 2,
              name: "修改用户",
              path: null,
              perms: "sys:user:update",
              component: null,
              type: 2,
              icon: null,
              ordernum: 2,
              children: [],
            },
            {
              id: 11,
              created: "2021-01-17T21:49:21",
              updated: null,
              statu: 1,
              parentId: 2,
              name: "删除用户",
              path: null,
              perms: "sys:user:delete",
              component: null,
              type: 2,
              icon: null,
              ordernum: 3,
              children: [],
            },
            {
              id: 12,
              created: "2021-01-17T21:49:58",
              updated: null,
              statu: 1,
              parentId: 2,
              name: "分配角色",
              path: null,
              perms: "sys:user:role",
              component: null,
              type: 2,
              icon: null,
              ordernum: 4,
              children: [],
            },
            {
              id: 13,
              created: "2021-01-17T21:50:36",
              updated: null,
              statu: 1,
              parentId: 2,
              name: "重置密码",
              path: null,
              perms: "sys:user:repass",
              component: null,
              type: 2,
              icon: null,
              ordernum: 5,
              children: [],
            },
          ],
        },
        {
          id: 3,
          created: "2021-01-15T19:03:45",
          updated: "2021-01-15T19:03:48",
          statu: 1,
          parentId: 1,
          name: "角色管理",
          path: "/sys/roles",
          perms: "sys:role:list",
          component: "sys/Role",
          type: 1,
          icon: "el-icon-rank",
          ordernum: 2,
          children: [],
        },
      ],
    },
    {
      id: 5,
      created: "2021-01-15T19:06:11",
      updated: null,
      statu: 1,
      parentId: 0,
      name: "系统工具",
      path: "",
      perms: "sys:tools",
      component: null,
      type: 0,
      icon: "el-icon-s-tools",
      ordernum: 2,
      children: [
        {
          id: 6,
          created: "2021-01-15T19:07:18",
          updated: "2021-01-18T16:32:13",
          statu: 1,
          parentId: 5,
          name: "数字字典",
          path: "/sys/dicts",
          perms: "sys:dict:list",
          component: "sys/Dict",
          type: 1,
          icon: "el-icon-s-order",
          ordernum: 1,
          children: [],
        },
      ],
    },
  ];
  const result: Result = {
    code: 200,
    msg: "获取菜单数据成功！",
    data: menus,
  };
  return result;
});

// 模拟后端回显菜单编辑信息
Mock.mock(RegExp("/sys/menu/info/*"), "get", () => {
  let infoData: TableData = {
    id: 1,
    statu: 1,
    parentId: 1,
    name: "角色管理",
    path: "/sys/roles",
    perms: "sys:role:list",
    component: "sys/Role",
    type: 1,
    icon: "el-icon-rank",
    orderNum: 2,
    children: [],
  };
  const result: Result = {
    code: 200,
    msg: "获取当前行成功！",
    data: infoData,
  };
  return result;
});

// 模拟后端菜单编辑请求
Mock.mock(RegExp("/sys/menu/update"), "post", () => {
  const result: Result = {
    code: 200,
    msg: "菜单更新成功",
    data: null,
  };
  return result;
});
Mock.mock(RegExp("/sys/menu/save"), "post", () => {
  const result: Result = {
    code: 200,
    msg: "菜单创建成功",
    data: null,
  };
  return result;
});

// 模拟后端删除菜单表格内容
Mock.mock(RegExp("/sys/menu/delete/*"), "post", () => {
  const result: Result = {
    code: 200,
    msg: "菜单删除成功！",
    data: null,
  };
  return result;
});

// 返回角色列表数据信息
Mock.mock(RegExp("/sys/role/list*"), "get", () => {
  const records: RoleListResponse = {
    records: [
      {
        id: 3,
        created: "2021-01-04T10:09:14",
        updated: "2021-01-30T08:19:52",
        statu: 1,
        name: "普通用户",
        code: "normal",
        describe: "只有基本查看功能",
        menuIds: [],
      },
      {
        id: 6,
        created: "2021-01-16T13:29:03",
        updated: "2021-01-17T15:50:45",
        statu: 1,
        name: "超级管理员",
        code: "admin",
        describe: "系统默认最高权限，不可以编辑和任意修改",
        menuIds: [],
      },
    ],
    total: 2,
    size: 10,
    current: 1,
    orders: [],
    optimizeCountSql: true,
    hitCount: false,
    countId: null,
    maxLimit: null,
    searchCount: true,
    pages: 1,
  };

  const result: Result = {
    code: 200,
    msg: "获取角色列表成功！",
    data: records,
  };

  return result; // 返回带有正确类型的Response对象
});

// 返回角色信息接口
Mock.mock(RegExp("/sys/role/info/*"), "get", () => {
  const role: RoleInfoResponse = {
    id: 6,
    created: "2021-01-16T13:29:03",
    updated: "2021-01-17T15:50:45",
    statu: 1,
    name: "超级管理员",
    code: "admin",
    describe: "系统默认最高权限，不可以编辑和任意修改",
    menuIds: [3],
  };

  const result: Result = {
    code: 200,
    msg: "获取用户信息成功！",
    data: role,
  };
  return result;
});

// 角色信息保存提交接口
Mock.mock("/sys/role/save", "post", () => {
  const result: Result = {
    code: 200,
    msg: "新增角色成功！",
    data: null,
  };

  return result;
});

// 角色信息修改提交接口
Mock.mock("/sys/role/update", "post", () => {
  const result: Result = {
    code: 200,
    msg: "修改角色成功！",
    data: null,
  };

  return result;
});

// 角色信息删除接口
Mock.mock(RegExp("/sys/role/delete*"), "post", () => {
  const result: Result = {
    code: 200,
    msg: "删除成功！",
    data: null,
  };
  return result;
});

// 角色信息修改接口
Mock.mock(RegExp("/sys/role/perm/*"), "post", () => {
  const result: Result = {
    code: 200,
    msg: "新增权限成功！",
    data: null,
  };
  return result;
});


interface UserInfo {
  id: number,
  created?: string,
  updated?: string,
  statu?: number,
  username: string,
  password: string
  avatar: string,
  email?: string,
  phone?: string,
  city?: string | null,
  lastLogin?: string | null,
  roles?: Role[]
}
interface ListInfo {
  records: UserInfo[],
  total: number,
  size: number,
  current: number,
  orders: any[], // 假设orders可以是任意类型的数组
  optimizeCountSql: boolean,
  hitCount: boolean,
  countId: null,
  maxLimit: null,
  searchCount: boolean,
  pages: number
}
/// 用户管理相关接口
Mock.mock(RegExp('/sys/user/list*'), 'get', () => {
  const listInfo: ListInfo = {
    "records": [
      {
        "id": 1,
        "created": "2021-01-12T22:13:53",
        "updated": "2021-01-16T16:57:32",
        "statu": 1,
        "username": "admin",
        "password": "$2a$10$R7zegeWzOXPw871CmNuJ6upC0v8D373GuLuTw8jn6NET4BkPRZfgK",
        "avatar": "https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/5a9f48118166308daba8b6da7e466aab.jpg",
        "email": "123@qq.com",
        "city": "广州",
        "lastLogin": "2020-12-30T08:38:37",
        "roles": [
          {
            "id": 6,
            "created": "2021-01-16T13:29:03",
            "updated": "2021-01-17T15:50:45",
            "statu": 1,
            "name": "超级管理员",
            "code": "admin",
            "describe": "系统默认最高权限，不可以编辑和任意修改",
            "menuIds": []
          },
          {
            "id": 3,
            "created": "2021-01-04T10:09:14",
            "updated": "2021-01-30T08:19:52",
            "statu": 1,
            "name": "普通用户",
            "code": "normal",
            "describe": "只有基本查看功能",
            "menuIds": []
          }
        ]
      },
      {
        "id": 2,
        "created": "2021-01-30T08:20:22",
        "updated": "2021-01-30T08:55:57",
        "statu": 1,
        "username": "test",
        "password": "$2a$10$0ilP4ZD1kLugYwLCs4pmb.ZT9cFqzOZTNaMiHxrBnVIQUGUwEvBIO",
        "avatar": "https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/5a9f48118166308daba8b6da7e466aab.jpg",
        "email": "test@qq.com",
        "city": null,
        "lastLogin": null,
        "roles": [
          {
            "id": 3,
            "created": "2021-01-04T10:09:14",
            "updated": "2021-01-30T08:19:52",
            "statu": 1,
            "name": "普通用户",
            "code": "normal",
            "describe": "只有基本查看功能",
            "menuIds": []
          }
        ]
      }
    ],
    "total": 2,
    "size": 10,
    "current": 1,
    "orders": [],
    "optimizeCountSql": true,
    "hitCount": false,
    "countId": null,
    "maxLimit": null,
    "searchCount": true,
    "pages": 1
  }
  const result: Result = {
    code: 200,
    msg: "获取用户列表成功！",
    data: listInfo
  }

  return result;
});

// 角色信息保存提交接口
Mock.mock("/sys/user/save", "post", () => {
  const result: Result = {
    code: 200,
    msg: "新增用户成功！",
    data: null,
  };

  return result;
});

// 用户信息修改提交接口
Mock.mock("/sys/user/update", "post", () => {
  const result: Result = {
    code: 200,
    msg: "修改用户成功！",
    data: null,
  };

  return result;
});

// 角色信息删除接口
Mock.mock(RegExp("/sys/user/delete*"), "post", () => {
  const result: Result = {
    code: 200,
    msg: "删除用户成功！",
    data: null,
  };
  return result;
});

// 用户信息的回显
Mock.mock(RegExp('/sys/user/info*'), 'get', () => {
  const userInfo: UserInfo = {
    "id": 2,
    "created": "2021-01-30T08:20:22",
    "updated": "2021-01-30T08:55:57",
    "statu": 1,
    "username": "test",
    "password": "$2a$10$0ilP4ZD1kLugYwLCs4pmb.ZT9cFqzOZTNaMiHxrBnVIQUGUwEvBIO",
    "avatar": "https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/5a9f48118166308daba8b6da7e466aab.jpg",
    "email": "test@qq.com",
    "phone": "13333333333",
    "city": null,
    "lastLogin": null,
    "roles": [
      {
        "id": 6,
        "created": "2021-01-16T13:29:03",
        "updated": "2021-01-17T15:50:45",
        "statu": 1,
        "name": "超级管理员",
        "code": "admin",
        "describe": "系统默认最高权限，不可以编辑和任意修改",
        "menuIds": []
      }, {
        "id": 3,
        "created": "2021-01-04T10:09:14",
        "updated": "2021-01-30T08:19:52",
        "statu": 1,
        "name": "普通用户",
        "code": "normal",
        "describe": "只有基本查看功能",
        "menuIds": []
      }]
  }
  const result: Result = {
    code: 200,
    msg: "获取用户信息成功！",
    data: userInfo
  }
  return result;
})

// 编辑用户角色
Mock.mock(RegExp('/sys/user/role/*'), 'post', () => {
  const result: Result = {
    code: 200,
    msg: "编辑用户角色成功！",
    data: null
  }

  return result;
})

// 重置用户密码
Mock.mock(RegExp('/sys/user/resetPas/'), 'post', () => {
  const result: Result = {
    code: 200,
    msg: "重置用户密码成功！",
    data: null
  }

  return result;
})