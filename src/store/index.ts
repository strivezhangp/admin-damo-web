import { createStore } from 'vuex';
import menus from './modules/menus'

// 定义状态的类型  
interface State {
  /**
   * 这是一个索引签名。
   * 它表示 State 对象的属性名可以是任何字符串，
   * 并且这些属性的值可以是任何类型（由 any 表示）。
   * 这是一个非常宽松的约束，
   * 允许你在 State 对象中添加任何属性和任何类型的值。
   */
  [x: string]: any;
  token: string;
}

// 创建 Vuex store  
const store = createStore<State>({
  state() {
    return {
      token: '',
    };
  },
  getters: {
    // 如果需要，可以在这里添加 getter  
  },
  mutations: {
    // 不能直接操作state中的token 所以在此进行操作
    // 设置state中的token方法
    SET_TOKEN(state, token: string) {
      state.token = token;
      localStorage.setItem("token", token);
    },
    // resetState(state) {
    //   // 清除 token 信息  
    //   state.token = '';
    //   // 清除一些权限信息等（如果有的话，可以在这里添加相应的逻辑）  
    //   localStorage.removeItem("token");
    // }
  },
  actions: {
    // 如果有异步操作，可以在这里添加 action  
  },
  modules: {
    // 引入菜单状态模块
    menus
  }
});

export default store;