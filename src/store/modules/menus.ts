// 引入全局的menus状态
// 实现导航栏的数据共享 使用store实现
// 状态管理，共享给后续页面

export default {
  state: {
    // 添加要共享的数据
    menuList: [],
    premList: [], // 权限列表
    // 定义一个全局状态 查看路由是否获取过 防止多次调用接口请求获取路由
    // 并将这个状态放到全局的session中保存
    hasRouter: false,

    // 标签页管理 默认显示一个首页
    editableTabs: [
      {
        title: "首页",
        name: "Index",
      },
    ],
    editableTabsValue: "Index",
  },

  /**
   *
   * 在 Vuex 中，mutations 是用来修改 state 的唯一途径，确保状态的可预测性和一致性。
   * mutations 是同步的，因此在调用 store.commit 后，状态会立即更新。这与 actions 不同，actions 可以包含异步操作。
   */
  mutations: {
    // 定义方法实现数据的修改
    setMenuList(state: any, menus: any) {
      state.menuList = menus;
    },

    setPermList(state: any, prems: any) {
      state.premList = prems;
    },
    changeHasRouter(state: any, hasRouter: any) {
      state.hasRouter = hasRouter;
    },
    // 修改当前激活标签
    setActiveTab(state: any, activeTab: any) {
      state.editableTabsValue = activeTab;
    },

    // 添加标签页事件
    addTab(state: any, tab: any) {
      // 将标签添加到数组中 判断是否已经在标签中
      let flag = state.editableTabs.some((item: any) => item.name === tab.name);
      if (!flag) {
        // 如果不在tabs中
        state.editableTabs.push({
          title: tab.title,
          name: tab.name,
        });
        // 修改当前表单为激活状态
        state.editableTabsValue = tab.name;
      } else {
        // 如果已经在tabs中 设置为激活状态
        state.editableTabsValue = tab.name;
      }
    },

    // 删除标签页
    removeTab(state: any, targetName: any) {
      let tabs = state.editableTabs;
      let activeName = state.editableTabsValue;

      // 判断是否为首页 不进行任何操作
      if (activeName === 'Index') {
        return;
      }

      // 如果删除的是当前已激活标签
      else if (activeName === targetName) {
        tabs.forEach((tab: any, index: any) => {
          // 从序列中循环找到该标签
          if (tab.name === targetName) {
            // 更换当前激活标签
            let nextTab = tabs[index + 1] || tabs[index - 1];
            if (nextTab) {
              activeName = nextTab.name;
            }
          }
        });
      }
      state.editableTabsValue = activeName;
      state.editableTabs = tabs.filter((tab: { name: any; }) => tab.name !== targetName);
    },
    // 用户登出后清空store中的tabs和menus以及用户信息
    resetState(state: any) {
      // 清除 信息  
      state.menuList = [];
      state.premList = [];
      state.hasRoute = false;
      state.editableTabs = [
        {
          title: "首页",
          name: "Index",
        },
      ];
      state.editableTabsValue = 'Index';
    }
  },
  actions: {},
};
