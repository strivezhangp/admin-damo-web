const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 配置1 关闭vue项目弹窗提示
  devServer: {
    host: 'localhost',
    port: 8088,
    client: {
      overlay: false
    }
  },
  // 配置1 end
})
